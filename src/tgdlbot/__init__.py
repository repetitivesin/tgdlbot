from hashlib import blake2b
import time
from random import randint
from typing import Any
from pyrogram import filters
from pyrogram.errors import FloodWait, RPCError
from pyrogram.client import Client
from pyrogram.errors.exceptions import bad_request_400
from pyrogram.handlers.message_handler import MessageHandler
from pyrogram.types import Message
from yt_dlp import YoutubeDL
from pathlib import Path
import os
import re
import asyncio
import logging
import json
import argparse
import atexit

logger = logging.getLogger(__name__)

regexes = {
    "tiktok": r"(?:https?://(?:m|vm|vt|www)\.tiktok\.com/\S*)",
    "reddit": r"(?:https?://(?:[^/]+\.)?reddit(?:media)?\.com/r/(?:[^/]+/comments/(?:[^/?#&]+)))",
    "instagram_reels": r"(?:https?://(?:www\.)?instagram\.com/reel/(?:[-a-zA-Z0-9]+)/(\?igsh=(?:[a-zA-Z0-9]+))?)",
}

def combine_regex(names: list[str]):
    return re.compile("|".join(map(lambda name: regexes[name], names))
                         .join(["(", ")"]))

COMBINED_REGEXES = combine_regex(["instagram_reels"])

class Options:
    credentials = "./credentials.json"
    tmpdir = "/tmp/tgdlbot"
    debug = False
    limit = 100

class Videos:
    def __init__(self, options: Options) -> None:
        self.options = options
        self.videos = {}
        self.video_limit = max(1, options.limit)
        self.ytdl_opts = {
            "outtmpl": f"{options.tmpdir}/%(id)s.%(ext)s",
            "quiet": "false" if options.debug else "true",
        }
        try:
            Path(options.tmpdir).mkdir(parents=True, mode=0o700, exist_ok=True)
        except Any as e:
            logger.error(f"Could not create directory {options.tmpdir}")
            raise e

    def prefetch(self, url) -> tuple [str, str, str]:
        """
        Prefetch basic metadata of a video behind url without downloading it.
        Create hash that serves as a unique ID for that video
        @returns (title, hash, target_filename)
        """
        with YoutubeDL(self.ytdl_opts) as ydl:
            info: dict[str, Any] = ydl.extract_info(url, download = False)
        extractor: str = info.get("extractor") or "generic"
        title: str = info.get("title", f"{extractor} video")
        filename: str = ydl.prepare_filename(info)
        id: str = str(info.get("id", 0))
        hash = blake2b(title.encode(), digest_size=20)
        hash.update(extractor.encode())
        hash.update(id.encode())
        return title, hash.hexdigest(), filename

    def download_new(self, url) -> str:
        """
        Download video, track it in `self.videos` and return it's hash
        @return hash
        """
        with YoutubeDL(self.ytdl_opts) as ydl:
            try:
                title, hash, filename = self.prefetch(url)
                logger.info("Video does not exist on local system, "
                         "downloading...")
                ydl.download(url)
            except Exception as e:
                logger.exception("failed to download video")
                raise e
            self.videos[hash] = {
                "title": title,
                "file": filename,
                "url": url,
                "download_time": time.time_ns(),
            }
            return hash

    def is_downloaded(self, hash) -> bool:
        """
        Check whether video is downloaded locally
        """
        vid = self.videos.get(hash, None)
        return vid and vid.get("file", None) and os.path.isfile(vid["file"])

    def was_uploaded_before(self, hash) -> bool:
        """
        Check whether video was uploaded before.
        This is different from checking whether we are tracking `file_id`
        because it can expire
        """
        vid = self.videos.get(hash, None)
        return vid and vid.get("file_id", None)

    def get_video_file(self, url) -> tuple [str, str]:
        """
        Get prepared file or file ID ready to be uploaded
        @returns (filename | file_id), hash
        """
        _, hash, filename = self.prefetch(url)
        if self.was_uploaded_before(hash):
            logger.info(f"Video(hash={hash}) exists on telegram servers")
            return self.videos[hash]["file_id"], hash
        elif not self.is_downloaded(hash):
            logger.info(
                f"Video(hash={hash}) is not downloaded, proceeding to download")
            self.download_new(url)
        return filename, hash

    def delete_video(self, hash: str, untrack: bool = False) -> None:
        vid = self.videos.get(hash)
        if not vid:
            logger.error(f"Video with hash {hash} does not exist")
        if vid.get('file') and Path(vid['file']).exists():
            logger.info(f"Deleting file hash={hash}; file={vid['file']}")
            os.remove(vid['file'])
            self.videos[hash].pop('file', None)
        if untrack:
            self.videos.pop(hash, None)

    def get_n_oldest(self, n: int) -> list [tuple [str, int]]:
        """
        Get n oldest tracked videos
        @return [(hash, download_time)]
        """
        videos_arr : list [tuple [str, int]] = []
        for k, v in self.videos.items():
            videos_arr.append((k, v["download_time"]))
        videos_arr_sorted = sorted(videos_arr, key=lambda tup: tup[1])
        return videos_arr_sorted[:n]

    def untrack_half_videos(self) -> None:
        """
        Stop tracking exactly half of currently tracked videos
        It is used to save RAM when runnig for a long time
        """
        n: int = len(self.videos)//2
        logger.info(f"Untracking {n} oldest videos")
        old_videos = self.get_n_oldest(n)
        for hash, _ in old_videos:
            self.delete_video(hash, untrack=True)

    def is_over_limit(self) -> bool:
        """
        Check whether more videos are tracked than the limit permits
        """
        return len(self.videos) > self.video_limit

    def cleanup(self) -> None:
        logger.info("Cleaning up the videos")
        for hash in list(self.videos.keys()):
            logger.info(f"untracking video hash={hash}")
            self.delete_video(hash, untrack=True)
        if Path(self.options.tmpdir).is_dir() and len(os.listdir(self.options.tmpdir)) == 0:
            os.rmdir(self.options.tmpdir)

parser = argparse.ArgumentParser("tgdlbot", description="Telegram bot that "
    "replies to messages containing URLs with videos behind those URLs")
parser.add_argument("-C", "--credentials", default="./credentials.json",
                    help="JSON credentials file")
parser.add_argument("-T", "--tmp-dir", dest="tmpdir", default="/tmp/tgdlbot",
                    help="Where to store temporary videos")
parser.add_argument("-D", "--debug", action="store_true",
                    help="Enable debugging(more logs and debug commands in chat)")
parser.add_argument("-L", "--limit", type=int, default=100,
                    help="How much videos to track at the same time?")

def run() -> None:
    options = Options()
    parser.parse_args(namespace=options)
    videos = Videos(options)
    logging.basicConfig(level=logging.DEBUG if options.debug else logging.INFO)
    with open(options.credentials, "r") as f:
        credentials = json.load(f)
    app = Client("tgdlbot",
                 api_id         = credentials["api_id"],
                 api_hash       = credentials["api_hash"],
                 phone_number   = credentials["phone_number"],
                 session_string = credentials["session_string"],
                 in_memory      = True)

    async def message_url_handler(client: Client, message: Message) -> None:
        """
        Filter links for `combined-regex` and download videos from there
        Reply to the message with downloaded video
        """
        urls: list[str] = re.findall(COMBINED_REGEXES, message.text)
        async def upload_video(url: str):
            file, hash = videos.get_video_file(url)
            info = videos.videos[hash]
            title = info['title'][0:1024]
            logger.debug(f"Sending video(hash={hash})")
            mes = await client.send_video(chat_id = message.chat.id,
                                          reply_to_message_id = message.id,
                                          video = file,
                                          disable_notification = True,
                                          # telegram limits caption size to 1024
                                          caption = title)
            if mes:
                videos.videos[hash]['file_id'] = mes.video.file_id
                videos.delete_video(hash)
        for url in urls:
            try:
                await upload_video(url)
            except FloodWait as e:
                await asyncio.sleep(e.value)
            except bad_request_400.FileReferenceExpired:
                logger.exception("file_id expired for video")
                _, hash, _ = videos.prefetch(url)
                videos.delete_video(hash, untrack=True)
                videos.download_new(url)
                await upload_video(url)
            except RPCError:
                logger.exception("Couldn't send file to Telegram")
        while videos.is_over_limit():
            videos.untrack_half_videos()

    app.add_handler(
            MessageHandler(
                message_url_handler,
                filters.text and filters.regex(COMBINED_REGEXES)))

    async def debug_10_oldest(client: Client, message: Message) -> None:
        await client.send_message(chat_id=message.chat.id,
                                  reply_to_message_id=message.id,
                                  text=str(videos.get_n_oldest(10)))

    async def debug_del_handler(client: Client, message: Message) -> None:
        videos.untrack_half_videos()
        await client.send_message(chat_id=message.chat.id,
                                  reply_to_message_id=message.id,
                                  text="DBG: untracked half of old videos")

    async def debug_is_over_limit(client: Client, message: Message) -> None:
        await client.send_message(chat_id=message.chat.id,
                                  reply_to_message_id=message.id,
                                  text=f"over limit = {videos.is_over_limit()}; limit = {videos.video_limit}")

    if options.debug:
        app.add_handler(
            MessageHandler(
                debug_10_oldest,
                filters.text and filters.regex(re.compile("/ten-oldest"))))

        app.add_handler(
            MessageHandler(
                debug_del_handler,
                filters.text and filters.regex(re.compile("/untrack"))))

        app.add_handler(
            MessageHandler(
                debug_is_over_limit,
                filters.text and filters.regex(re.compile("/overlimit"))))

    app.run()
    atexit.register(videos.cleanup)
