{
  description = "Python bot for telegram that sends videos as a response to message with link";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    poetry2nix.url = "github:nix-community/poetry2nix";
    poetry2nix.inputs.nixpkgs.follows = "nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; })
          mkPoetryApplication;

        app = mkPoetryApplication {
          projectDir = ./.;
          preferWheels = true;
        };

        packageName = "tgdlbot";
      in
      {
        packages.${packageName} = app;

        packages.default = self.packages.${system}.${packageName};

        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [ poetry ];
          inputsFrom = [ self.packages.${system} ];
        };

        nixosModules.default = self.nixosModules.tgdlbot;
        nixosModules.tgdlbot = { config, lib, ... }: {
          options.services.tgdlbot = {
            enable = lib.mkEnableOption "tgdlbot" // {
              description = ''
                Whether to enable tgdlbot, a Python bot for telegram that sends
                videos as a response to message with link.
              '';
            };
            limit = lib.mkOption {
              type = lib.types.int;
              default = 100;
              description = ''
                How much videos to track in memory. This stores only metadata,
                no videos are stored as they are removed immediately after
                uploading them to telegram servers, then bot receives file_id
                which is used instead when uploading.
              '';
            };
            credentialsFile = lib.mkOption {
              type = lib.types.path;
              default = null;
              description = ''
                A JSON file containing object that constist of api_id, api_hash,
                phone_number, session string
              '';
            };
            tmpDir = lib.mkOption {
              type = lib.types.path;
              default = "/tmp/tgdlbot";
              description = ''
                Where to download videos.
              '';
            };
            user = lib.mkOption {
              type = lib.types.str;
              default = "tgdlbot";
              description = "User account under which tgdlbot runs.";
            };
            group = lib.mkOption {
              type = lib.types.str;
              default = "tgdlbot";
              description = "Group account under which tgdlbot runs.";
            };
          };

          config =
            let cfg = config.services.tgdlbot;
            in lib.mkIf cfg.enable {
              systemd.services.tgdlbot = {
                description = "tgdlbot telegram bot";
                after = [ "network.target" ];
                wantedBy = [ "multi-user.target" ];
                serviceConfig = {
                  ExecStart = /*bash */''
                    ${app}/bin/tgdlbot\
                      --credentials "${cfg.credentialsFile}"\
                      --tmp-dir "${cfg.tmpDir}"\
                      --limit "${builtins.toString cfg.limit}"
                  '';
                  User = cfg.user;
                  Group = cfg.group;
                  PrivateDevices = true;
                  ProtectControlGroups = true;
                  ProtectHome = true;
                  ProtectKernelTunables = true;
                  ProtectSystem = "full";
                  PrivateUsers = true;
                  MountAPIVFS = true;
                  ProtectKernelLogs = true;
                  ProtectProc = "invisible";
                };
              };
              users.users = lib.optionalAttrs (cfg.user == "tgdlbot") ({
                tgdlbot = {
                  group = cfg.group;
                  uid = 351;
                  description = "tgdlbot telegram bot";
                  isSystemUser = true;
                };
              });

              users.groups = lib.optionalAttrs (cfg.group == "tgdlbot") ({
                tgdlbot = {
                  gid = 351;
                };
              });
            };
        };
      });
}
